# Let's use centos as the base image.
FROM centos

RUN yum -y install python3
RUN dnf install gcc-c++ python3-devel
RUN pip3 install tensorflow --user

COPY unittest.sh /tmp/unittest.sh

RUN chmod +x /tmp/unittest.sh
